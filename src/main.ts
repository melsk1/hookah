import { createApp } from 'vue'
import { createPinia } from "pinia";
import App from './App.vue'
import "./assets/scss/global.scss";
import "./assets/scss/variables.scss";
import { createMetaManager } from 'vue-meta'

const app = createApp(App);
const pinia = createPinia();


app.use(pinia).use(createMetaManager()).mount("#app");